package com.talan.gto.ZuulService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;

import com.talan.gto.ZuulService.filter.ErrorFilter;
import com.talan.gto.ZuulService.filter.PostFilter;
import com.talan.gto.ZuulService.filter.PreFilter;
import com.talan.gto.ZuulService.filter.RouteFilter;



@SpringBootApplication
@EnableDiscoveryClient
@EnableZuulProxy
public class GtoZuulServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(GtoZuulServiceApplication.class, args);
	}

	@Bean
	public PreFilter preFilter() {
		return new PreFilter();
	}

	@Bean
	public PostFilter postFilter() {
		return new PostFilter();
	}

	@Bean
	public ErrorFilter errorFilter() {
		return new ErrorFilter();
	}

	@Bean
	public RouteFilter routeFilter() {
		return new RouteFilter();
	}
}
